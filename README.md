## basic_package_2 (kinetic) - 0.0.3-0

The packages in the `basic_package_2` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release basic_package_2 --track kinetic --rosdistro kinetic` on `Tue, 26 Jun 2018 06:51:14 -0000`

The `basic_package_2` package was released.

Version of package(s) in repository `basic_package_2`:

- upstream repository: https://gitlab.com/asierfernandez/basic_package_2.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.0.2-1`
- new version: `0.0.3-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.3`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## basic_package_2 (kinetic) - 0.0.2-1

The packages in the `basic_package_2` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release basic_package_2 --track kinetic --rosdistro kinetic` on `Mon, 25 Jun 2018 11:30:48 -0000`

The `basic_package_2` package was released.

Version of package(s) in repository `basic_package_2`:

- upstream repository: https://gitlab.com/asierfernandez/basic_package_2.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.0.2-0`
- new version: `0.0.2-1`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.3`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## basic_package_2 (kinetic) - 0.0.2-0

The packages in the `basic_package_2` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release basic_package_2 --track kinetic --rosdistro kinetic` on `Mon, 25 Jun 2018 07:42:53 -0000`

The `basic_package_2` package was released.

Version of package(s) in repository `basic_package_2`:

- upstream repository: https://gitlab.com/asierfernandez/basic_package_2.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.0.1-0`
- new version: `0.0.2-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.3`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## basic_package (kinetic) - 0.0.1-0

The packages in the `basic_package` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release basic_package --track kinetic --rosdistro kinetic` on `Fri, 22 Jun 2018 06:39:07 -0000`

The `basic_package` package was released.

Version of package(s) in repository `basic_package`:

- upstream repository: https://gitlab.com/asierfernandez/basic_package_2.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.0.1-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.3`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


